public class ShutTheBox{
  public static void main(String[] args){
    System.out.println("Welcome to the java game Shut the Box! Hope you're ready to have fun.");
    
    Board gameBoard = new Board();
    boolean gameOver = false;
    
    while (gameOver == false) {
      System.out.println("Player 1's turn");
      System.out.println(gameBoard);
      if (gameBoard.playATurn() == true) {
        gameOver = true;
        System.out.println("Player 2 wins! Congrats");
      }
      else {
        System.out.println("Player 2's turn");
        System.out.println(gameBoard);
        if (gameBoard.playATurn() == true){
          gameOver = true;
          System.out.println("Player 1 wins! Congrats");
        }
      }
    }
  }
}