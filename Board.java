public class Board {
  private Die die1;
  private Die die2;
  private boolean[] closedTiles;
  
  public Board(){
    this.die1 = new Die();
    this.die2 = new Die();
    this.closedTiles = new boolean[12];
  }
  
  public String toString(){
    String string = "";
    for (int i = 0; i < this.closedTiles.length; i++){
      if (this.closedTiles[i] == false){
        string = string + (i+1) + " ";
      }
      else {
        string = string + "X ";
      }
    }
    return string;
  }
  
  public boolean playATurn(){
    this.die1.roll();
    this.die2.roll();
    System.out.println("You rolled a " + this.die1.toString() + " and a " + this.die2.toString() + ".");
    int sum = die1.getPips() + die2.getPips();
    System.out.println("The sum of those rolls is "+ sum + ".");
    
    if (this.closedTiles[sum -1] == false){
      System.out.println("Closing tile: "+ (sum));
      this.closedTiles[sum-1] = true;
      return false;
    }
    else {
      System.out.println("This tile is already closed");
      return true;
    }
  }
}
