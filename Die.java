import java.util.Random;

public class Die {
  private int pips;
  private Random rng;
  
  public Die() {
    this.pips = 1;
    this.rng = new Random();
  }
  
  public int getPips(){
    return this.pips;
  }
   
  public Random getRoll(){
    return this.rng;
  }
  
  public void roll(){
    this.pips = this.rng.nextInt(6) + 1;
  }
  
  public String toString(){
    String string = this.pips + "";
    return string;
  }
}